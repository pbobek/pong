package com.bobek.piotr.pong.client;

import com.bobek.piotr.pong.model.Ball;
import com.bobek.piotr.pong.model.IBall;
import com.bobek.piotr.pong.model.IPad;
import com.bobek.piotr.pong.model.IPadCalculations;
import com.bobek.piotr.pong.model.LeftPadCalculations;
import com.bobek.piotr.pong.model.Pad;
import com.bobek.piotr.pong.model.RightPadCalculations;
import com.bobek.piotr.pong.shared.PointWinner;
import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.media.client.Audio;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Pong implements EntryPoint {

	private final int CANVAS_WIDTH = 800;
	private final int CANVAS_HEIGHT = 700;

	private final int BALL_WIDTH = 25;
	private final int BALL_HEIGHT = 25;
	private final int BALL_SPEED = 10;
	private final int START_ANGLE = 180;

	private final int PAD_SPEED = 5;
	private final int PAD_WIDTH = 10;
	private final int PAD_HEIGHT = 150;
	private final int PAD_GAP = 50;

	private final int POINTS_FOR_WIN = 10;

	private Canvas canvas;
	private Context2d context;
	private IBall ball;
	private IPad leftPad;
	private IPad rightPad;
	private int computersPoint = 0;
	private int playersPoint = 0;
	private Audio padCollisionSound;
	private Audio wallCollisionSound;
	private Audio scoreSound;

	private HandlerRegistration buttonsHandler;
	private HandlerRegistration controllHandler;
	private HandlerRegistration keyUpHandler;	

	@Override
	public void onModuleLoad() {
		canvas = Canvas.createIfSupported();

		if (canvas == null) {
			RootPanel
					.get()
					.add(new Label(
							"Twoja przeglądarka nie obsługuje elementu CANVAS"));
			return;
		}

		prepareCanvas();
		RootPanel.get().add(canvas);
		showStartScreen();
	}

	public void loadSounds() {
		padCollisionSound = Audio.createIfSupported();
		wallCollisionSound = Audio.createIfSupported();
		scoreSound = Audio.createIfSupported();

		padCollisionSound.setSrc("sounds/padCollision.ogg");
		wallCollisionSound.setSrc("sounds/wallCollision.ogg");
		scoreSound.setSrc("sounds/score.ogg");
	}

	private void prepareCanvas() {
		canvas.setStyleName("mainCanvas");
		canvas.setWidth(CANVAS_WIDTH + "px");
		canvas.setCoordinateSpaceWidth(CANVAS_WIDTH);

		canvas.setHeight(CANVAS_HEIGHT + "px");
		canvas.setCoordinateSpaceHeight(CANVAS_HEIGHT);

		context = canvas.getContext2d();
		context.setFillStyle("white");
		context.setFont("30pt Calibri");
		context.setTextAlign("center");
		context.setStrokeStyle("red");
		context.save();
	}

	private void showStartScreen() {
		final int buttonWidth = 250;
		final int buttonHeight = 150;
		final int buttonY = 250;
		final int buttonKeyboardX = 100;
		final int buttonMouseX = CANVAS_WIDTH - 100 - buttonWidth;

		context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
		context.fillText("Pong Piotr Bobek", CANVAS_WIDTH / 2, 100);

		context.setFont("15pt Calibri");
		context.fillText("Wybierz sterowanie", CANVAS_WIDTH / 2, 225);

		context.strokeRect(buttonKeyboardX, buttonY, buttonWidth, buttonHeight);
		context.fillText("Klawiatura", buttonKeyboardX + 125, buttonY
				+ buttonHeight / 2);

		context.strokeRect(buttonMouseX, buttonY, buttonWidth, buttonHeight);
		context.fillText("Myszka", buttonMouseX + 125, buttonY + buttonHeight
				/ 2);
		context.restore();

		buttonsHandler = canvas.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int mouseX = event.getX();
				int mouseY = event.getY();

				if (mouseX >= buttonKeyboardX
						&& mouseX <= (buttonKeyboardX + buttonWidth)
						&& mouseY >= buttonY
						&& mouseY <= (buttonY + buttonHeight)) {
					startGame(true);
					buttonsHandler.removeHandler();
				} else if (mouseX >= buttonMouseX
						&& mouseX <= (buttonMouseX + buttonWidth)
						&& mouseY >= buttonY
						&& mouseY <= (buttonY + buttonHeight)) {
					startGame(false);
					buttonsHandler.removeHandler();
				}
			}
		});
	}

	private void showRestartScreen(PointWinner pointWinner) {
		final int buttonWidth = 250;
		final int buttonHeight = 150;
		final int buttonY = 250;
		final int buttonX = CANVAS_WIDTH / 2 - buttonWidth / 2;

		String text;
		if (pointWinner == PointWinner.COMPUTER) {
			text = "Przegrana!";
		} else {
			text = "Wygrana!";
		}

		clearCanvas();
		context.fillText("Pong Piotr Bobek", CANVAS_WIDTH / 2, 100);
		context.strokeText(text, CANVAS_WIDTH / 2, 175);

		context.setFont("15pt Calibri");
		context.setStrokeStyle("red");

		context.strokeRect(buttonX, buttonY, buttonWidth, buttonHeight);
		context.fillText("Restart", buttonX + 125, buttonY + buttonHeight / 2);
		context.restore();

		buttonsHandler = canvas.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int mouseX = event.getX();
				int mouseY = event.getY();

				if (mouseX >= buttonX && mouseX <= (buttonX + buttonWidth)
						&& mouseY >= buttonY
						&& mouseY <= (buttonY + buttonHeight)) {
					buttonsHandler.removeHandler();
					computersPoint = 0;
					playersPoint = 0;
					showStartScreen();
				}
			}
		});
	}

	private void newRound() {
		IPadCalculations leftPadCalculations = new LeftPadCalculations(
				PAD_WIDTH, PAD_HEIGHT, PAD_GAP, CANVAS_WIDTH, CANVAS_HEIGHT,
				BALL_WIDTH, BALL_HEIGHT, PAD_SPEED);
		IPadCalculations rightPadCalculations = new RightPadCalculations(
				PAD_WIDTH, PAD_HEIGHT, PAD_GAP, CANVAS_WIDTH, CANVAS_HEIGHT,
				BALL_WIDTH, BALL_HEIGHT, PAD_SPEED);

		ball = new Ball(BALL_WIDTH, BALL_HEIGHT, START_ANGLE, BALL_SPEED,
				CANVAS_WIDTH, CANVAS_HEIGHT, BALL_WIDTH);
		leftPad = new Pad(leftPadCalculations);
		rightPad = new Pad(rightPadCalculations);

		final Timer timer = new Timer() {
			@Override
			public void run() {
				checkBallCollisionWithPads();
				checkBallCollisionWithWall();
				if (checkScore(this))
					return;

				move();
				drawElements();
			}
		};
		timer.scheduleRepeating(1000 / 60);
	}

	private void startGame(boolean useKeyboard) {
		loadSounds();

		if (controllHandler != null) {
			controllHandler.removeHandler();
		}

		if (keyUpHandler != null) {
			keyUpHandler.removeHandler();
		}

		if (useKeyboard) {
			bindKeyboardControls();
		} else {
			bindMouseControls();
		}

		context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
		newRound();
	}

	private boolean gameOver() {
		if (computersPoint == POINTS_FOR_WIN || playersPoint == POINTS_FOR_WIN) {
			return true;
		}

		return false;
	}

	private void drawText() {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(playersPoint);
		stringBuilder.append(" : ");
		stringBuilder.append(computersPoint);
		context.setFont("30pt Calibri");
		String text = stringBuilder.toString();
		context.fillText(text, CANVAS_WIDTH / 2, 100);
	}

	private void checkBallCollisionWithPads() {
		double leftAngle = leftPad.getAngleIfWasColisionWithBall(ball.getX(),
				ball.getY());
		double rightAngle = rightPad.getAngleIfWasColisionWithBall(ball.getX(),
				ball.getY());

		if (leftAngle != -1) {
			ball.setAngle(leftAngle);
			ball.setVelocity();
			padCollisionSound.play();
		} else if (rightAngle != -1) {
			ball.setAngle(rightAngle);
			ball.setVelocity();
			padCollisionSound.play();
		}
	}

	private void checkBallCollisionWithWall() {
		boolean wasWallCollision = ball.checkWallCollision();
		if (wasWallCollision) {
			wallCollisionSound.play();
		}
	}

	private boolean checkScore(Timer t) {
		PointWinner pointWinner = ball.whoScored();
		if (pointWinner == PointWinner.COMPUTER
				|| pointWinner == PointWinner.PLAYER) {

			scoreSound.play();

			switch (pointWinner) {
			case COMPUTER:
				computersPoint++;
				break;
			case PLAYER:
				playersPoint++;
				break;
			}

			ball.restart();
			leftPad.restart();
			rightPad.restart();

			if (gameOver()) {
				t.cancel();
				showRestartScreen(pointWinner);
				return true;
			}
		}

		return false;
	}

	private void drawElements() {
		clearCanvas();
		ball.draw(context);
		leftPad.draw(context);
		rightPad.draw(context);
		drawText();
	}

	private void move() {
		rightPad.setYDest((int) ball.getY());
		rightPad.move();
		leftPad.move();
		ball.move();
	}

	private void clearCanvas() {
		context.clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
	}

	private void bindMouseControls() {
		controllHandler = canvas.addMouseMoveHandler(new MouseMoveHandler() {
			@Override
			public void onMouseMove(MouseMoveEvent event) {
				int yCoord = event.getY();				
					leftPad.setYDest(yCoord);				
			}
		});
	}

	private void bindKeyboardControls() {
		controllHandler = canvas.addKeyDownHandler(new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				int key = event.getNativeKeyCode();
				switch (key) {
				case 38:
					leftPad.setYDest(0);
					break;

				case 40:
					leftPad.setYDest(CANVAS_HEIGHT);
				}
			}
		});

		keyUpHandler = canvas.addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				int key = event.getNativeKeyCode();
				switch (key) {
				case 38:
				case 40:
					leftPad.setInMove(false);
				}
			}
		});
	}

}
