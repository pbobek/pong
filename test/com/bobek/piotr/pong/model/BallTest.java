package com.bobek.piotr.pong.model;

import com.bobek.piotr.pong.shared.PointWinner;
import com.google.gwt.junit.client.GWTTestCase;

public class BallTest extends GWTTestCase {	

	@Override
	public String getModuleName() {
		return "com.bobek.piotr.pong.model.Ball";
	}

	int speed = 1;
	int canvasWidth = 600;
	int canvasHeight = 600;
	int ballHeight = 50;
	int ballWidth = 50;
	final int startAngle = 0;

	public void testCalculateXVelocityForSpeed1AndCanvas600x600ShouldReturn1() {

		Ball ball = new Ball(ballWidth, ballHeight, startAngle, speed, canvasWidth, canvasHeight, ballWidth);

		double expectedValue = 1;
		double actual = ball.calculateXVelocity();

		assertEquals(expectedValue, actual, 0.1);
	}

	public void testCalculateYVelocityForSpeed1AndCanvas600x600ShouldReturn0() {

		Ball ball = new Ball(ballWidth, ballHeight, startAngle, speed, canvasWidth, canvasHeight, ballWidth);

		double expectedValue = 0;
		double actual = ball.calculateYVelocity();

		assertEquals(expectedValue, actual, 0.1);
	}

}
