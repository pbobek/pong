package com.bobek.piotr.pong.model;

public interface IPad extends IPaintable{
	double getAngleIfWasColisionWithBall(double ballX, double ballY);
	void setY(double y);
	void setX(double x);		
	void setYDest(int cord);
	void setInMove(boolean inMove);
	void move();
	void restart();
}
