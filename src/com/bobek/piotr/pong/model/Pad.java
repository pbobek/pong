package com.bobek.piotr.pong.model;

import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.user.client.ui.Image;

public class Pad implements IPad {

	private final Image IMAGE;
	
	private ImageElement imageElement;
	private IPadCalculations padCalculations;

	public Pad(IPadCalculations padCalculations) {
		
		this.padCalculations = padCalculations;
		this.IMAGE = new Image("/img/pad.png");
		imageElement = ImageElement.as(IMAGE.getElement());

		restart();
	}

	@Override
	public Context2d draw(Context2d context) {	
	
		double x = padCalculations.getX();
		double y = padCalculations.getY();
		int width = padCalculations.getWIDTH();
		int height = padCalculations.getHEIGHT();
		
		
		context.drawImage(imageElement, x, y, width, height);
		return context;
	}

	public void restart() {
		padCalculations.restart();
	}

	/**
	 * 
	 * @param ballX
	 * @param ballY
	 * @return zwraca -1 gdy nie było kolizji
	 */
	public double getAngleIfWasColisionWithBall(double ballX, double ballY) {
		return padCalculations.getAngleIfWasColisionWithBall(ballX, ballY);
	}

	@Override
	public void move() {
		padCalculations.move();
	}

	public double getY() {
		return padCalculations.getY();
	}

	public void setY(double y) {
		padCalculations.setY(y);
	}

	public double getX() {
		return padCalculations.getX();
	}

	public void setX(double x) {
		padCalculations.setX(x);
	}

	public void setYDest(int cord) {
		padCalculations.setYDest(cord);
		padCalculations.setInMove(true);
	}

	public int getWIDTH() {
		return padCalculations.getWIDTH();
	}

	@Override
	public void setInMove(boolean inMove) {
		padCalculations.setInMove(inMove);
	}

}
