package com.bobek.piotr.pong.model;

import com.bobek.piotr.pong.shared.PointWinner;

public interface IBall extends IPaintable{
	
	/** Oblicza prędkość piłki na osi x
	 * 
	 * @return prędkośc piłki na osi x
	 */
	double calculateXVelocity();
	
	/** Oblicza prędkość piłki na osi y
	 * 
	 * @return prędkośc piłki na osi y
	 */
	double calculateYVelocity();	
	
	/** Sprawdź czy była kolizja ze ścianą
	 * 
	 * @return true -  była kolizja, false - brak kolizji
	 */
	boolean checkWallCollision();
	
	/** Sprawdza czy któryś z graczy zdobył punkt
	 * 
	 * @return zwraca Player - jeśli punkt zdobył gracz, Computer - jeśli komputer lub None -  jeśli nikt
	 */
	PointWinner whoScored();
	
	/** Restartuje obiekt do pozycji startowej
	 * 
	 */
	void restart();
	
	/** Przesuwa piłkę
	 * 
	 */
	public void move();
	
	double getX();
	double getY();
	void setAngle(double angle);void setVelocity();
}
