package com.bobek.piotr.pong.model;

import com.google.gwt.canvas.dom.client.Context2d;

public interface IPaintable {
	
	/** Rysuje obiekt
	 * 
	 * @param context context na którym odbędzie się rysowanie
	 * @return zwraca zmodyfikowany context
	 */
	Context2d draw(Context2d context);
}
