package com.bobek.piotr.pong.model;

import com.bobek.piotr.pong.shared.PointWinner;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.user.client.ui.Image;

public class Ball implements IBall {

	private final int WIDTH;
	private final int HEIGHT;

	private final double SPEED;
	private final int START_ANGLE;
	private final int CANVAS_WIDTH;
	private final int CANVAS_HEIGHT;
	private final int BALL_WIDTH;
	private final Image IMAGE;

	private double x;
	private double y;
	private double angle;

	private double xVelocity;
	private double yVelocity;
	private ImageElement imageElement;

	public Ball(int width, int height, int startAngle, int speed,
			int canvasWidth, int canvasHeight, int ballWidth) {
		this.WIDTH = width;
		this.HEIGHT = height;
		this.START_ANGLE = startAngle;
		this.angle = START_ANGLE;
		this.SPEED = speed;
		this.CANVAS_WIDTH = canvasWidth;
		this.CANVAS_HEIGHT = canvasHeight;
		this.BALL_WIDTH = ballWidth;
		
		//należy zakomentować wraz z deklaracją podczas testowania 
		this.IMAGE = new Image("/img/ball.png");
		imageElement = ImageElement.as(IMAGE.getElement());

		setPosition();

		xVelocity = calculateXVelocity();
		yVelocity = calculateYVelocity();
	}

	private void setPosition() {
		this.x = CANVAS_WIDTH / 2 - (WIDTH / 2);
		this.y = CANVAS_HEIGHT / 2 - (HEIGHT / 2);
	}

	@Override
	public void restart() {
		setPosition();
		angle = START_ANGLE;
		xVelocity = calculateXVelocity();
		yVelocity = calculateYVelocity();
	}

	@Override
	public Context2d draw(Context2d context) {

		context.drawImage(imageElement, x, y, WIDTH, HEIGHT);
		return context;
	}

	@Override
	public boolean checkWallCollision() {
		if (y + HEIGHT >= CANVAS_HEIGHT || y <= 0) {
			yVelocity *= -1;
			return true;
		}
		return false;
	}

	public void setVelocity() {
		xVelocity = calculateXVelocity();
		yVelocity = calculateYVelocity();
	}

	public double calculateXVelocity() {
		double angleInRadians = this.calculateRadians();
		return SPEED * Math.cos(angleInRadians);
	}

	public double calculateYVelocity() {
		double angleInRadians = this.calculateRadians();
		return SPEED * Math.sin(angleInRadians);
	}

	private double calculateRadians() {
		return angle / (180 / Math.PI);
	}

	@Override
	public PointWinner whoScored() {
		if (x <= 0) {
			return PointWinner.COMPUTER;
		} else if ((x + BALL_WIDTH) >= CANVAS_WIDTH) {
			return PointWinner.PLAYER;
		} else {
			return PointWinner.NONE;
		}
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getAngle() {
		return angle;
	}

	public void setAngle(double angle) {
		this.angle = angle;
	}

	public void move() {
		x += xVelocity;
		y += yVelocity;
	}
}
