package com.bobek.piotr.pong.model;

public class LeftPadCalculations implements IPadCalculations {

	private final int WIDTH;
	private final int HEIGHT;
	private final int GAP;
	private final int CANVAS_WIDTH;
	private final int CANVAS_HEIGHT;
	private final int BALL_HEIGHT;
	private final int BALL_WIDTH;
	private final int MAX_BOUNCE_ANGLE = 60;
	private final int SPEED;

	private double y;
	private double x;
	private double yDest;
	private boolean inMove;

	public LeftPadCalculations(int width, int height, int gap, int canvasWidth,
			int canvasHeight, int ballWidth, int ballHeight, int speed) {

		this.WIDTH = width;
		this.HEIGHT = height;
		this.GAP = gap;
		this.CANVAS_WIDTH = width;
		this.CANVAS_HEIGHT = canvasHeight;
		this.BALL_WIDTH = ballWidth;
		this.BALL_HEIGHT = ballHeight;
		this.SPEED = speed;

		restart();

	}

	public double getAngleIfWasColisionWithBall(double ballX, double ballY) {

		double halfOfPad = HEIGHT / 2; // polowa wysokosci paletki
		boolean yCollision = (ballY >= y || (ballY + BALL_HEIGHT) >= y);
		double halfOfBall = (double) BALL_HEIGHT / 2;

		// uderzenie dolnym rogiem piłki
		boolean botCornerCollision = (y >= (halfOfBall + ballY) && (ballY < y));

		// uderzenie górnym rogiem piłki
		boolean upCornerCollision = ((y + HEIGHT) < (halfOfBall + ballY));

		double pointOfYCollision;

		// ustawie punkt kolizji w zależności od tego czym uderzyła piłka

		if (botCornerCollision) {
			pointOfYCollision = ballY + BALL_HEIGHT;
		} else if (upCornerCollision) {
			pointOfYCollision = ballY - BALL_HEIGHT;
		} else {
			pointOfYCollision = ballY + halfOfBall;
		}

		if (ballX <= (x + WIDTH) && yCollision) {
			

			// dolna część pada
			if (pointOfYCollision > (halfOfPad + y)
					&& pointOfYCollision <= (HEIGHT + y)) {
				double pointOfColision = pointOfYCollision - halfOfPad - y;

				return (pointOfColision * MAX_BOUNCE_ANGLE) / halfOfPad;
			}
			// górna część pada
			else if (pointOfYCollision < (halfOfPad + y)) {
				double pointOfColision = y + halfOfPad - pointOfYCollision;

				return 360 - ((pointOfColision * MAX_BOUNCE_ANGLE) / halfOfPad);
			} else if (pointOfYCollision == (y + halfOfPad)) {
				return 0;
			}
		}
		// brak kolizji
		else {
			return -1;
		}

		return -1;

	}

	@Override
	public void move() {
		if (inMove) {
			if (yDest < y && y > 0) {
				y -= SPEED;
			} else if (yDest > y && y + HEIGHT < CANVAS_HEIGHT) {
				y += SPEED;
			}
		}
	}

	public void restart() {
		this.inMove = false;

		x = GAP;
		y = (CANVAS_HEIGHT - HEIGHT) / 2;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getyDest() {
		return yDest;
	}

	public void setyDest(double yDest) {
		this.yDest = yDest;
	}

	public boolean isInMove() {
		return inMove;
	}

	public void setInMove(boolean inMove) {
		this.inMove = inMove;
	}

	public int getWIDTH() {
		return WIDTH;
	}

	public int getHEIGHT() {
		return HEIGHT;
	}

	@Override
	public void setYDest(int cord) {
		this.yDest = cord;
	}

}
