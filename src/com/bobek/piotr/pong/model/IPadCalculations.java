package com.bobek.piotr.pong.model;

public interface IPadCalculations {

	/**Porusza padem
	 * 
	 */
	void move();
	
	/** Restartuje pad do początkowej pozycji
	 * 
	 */
	void restart();
	
	/** Zwraca kąt z jakim piłka powinna się odbić
	 * 
	 * @param ballX x piłki
	 * @param ballY y piłki
	 * @return wartość kąta odbicia lub -1 gdy nie było kolizji
	 */
	double getAngleIfWasColisionWithBall(double ballX, double ballY);
	
	double getX();
	double getY();
	void setX(double x);
	void setY(double y);
	
	void setYDest(int cord);
	int getWIDTH();
	int getHEIGHT();
	void setInMove(boolean inMove);
	
}
