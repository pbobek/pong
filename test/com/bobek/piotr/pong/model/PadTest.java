package com.bobek.piotr.pong.model;

import com.google.gwt.junit.client.GWTTestCase;

public class PadTest extends GWTTestCase {

	@Override
	public String getModuleName() {
		return null;
	}

	public void testLeftPadColisionWithBallForBounceInMidShouldReturn0()
	{		
		int padWidth = 50;
		int padHeight = 150;
		int gap = 50;
		int padSpeed = 10;
		int canvasHeight = 400;
		int canvasWidth = 600;
		int ballWidth = 25;
		int ballHeight = 25;
		
		double ballX = 50;
		double ballY = 187.5;
		IPadCalculations leftPadCalculations = new LeftPadCalculations(padWidth, padHeight, gap, 
				canvasWidth, canvasHeight, ballWidth, ballHeight,padSpeed);
		Pad pad = new Pad(leftPadCalculations);
		
		pad.setX(200 - padWidth);
		pad.setY(125);
		
		double expected = 0;
		double actual = pad.getAngleIfWasColisionWithBall(ballX, ballY);
		
		assertEquals(expected, actual, 0.1);
	}

	public void testLeftPadColisionWithBallForBounceInBottomMidShouldReturn30() {
		int padWidth = 50;
		int padHeight = 150;
		int gap = 50;
		int padSpeed = 10;
		int canvasHeight = 400;
		int canvasWidth = 600;
		int ballWidth = 25;
		int ballHeight = 25;

		double ballX = 50;
		double ballY = 225;

		IPadCalculations leftPadCalculations = new LeftPadCalculations(padWidth, padHeight, gap, 
				canvasWidth, canvasHeight, ballWidth, ballHeight,padSpeed);
		Pad pad = new Pad(leftPadCalculations);

		pad.setX(200 - padWidth);
		pad.setY(125);

		double expected = 30;
		double actual = pad.getAngleIfWasColisionWithBall(ballX, ballY);

		assertEquals(expected, actual, 1);
	}

	public void testLeftPadColisionWithBallForBounceInUpEndShouldReturn300() {
		int padWidth = 50;
		int padHeight = 150;
		int gap = 50;
		int padSpeed = 10;
		int canvasHeight = 400;
		int canvasWidth = 600;
		int ballWidth = 25;
		int ballHeight = 25;

		double ballX = 50;
		double ballY = 200;

		IPadCalculations leftPadCalculations = new LeftPadCalculations(padWidth, padHeight, gap, 
				canvasWidth, canvasHeight, ballWidth, ballHeight,padSpeed);
		Pad pad = new Pad(leftPadCalculations);

		pad.setX(200 - padWidth);
		pad.setY(225);

		double expected = 300;
		double actual = pad.getAngleIfWasColisionWithBall(ballX, ballY);

		assertEquals(expected, actual, 0.1);
	}

	public void testRightPadColisionWithBallForBounceInMidShouldReturn180() {
		int padWidth = 50;
		int padHeight = 150;
		int gap = 50;
		int padSpeed = 10;
		int canvasHeight = 400;
		int canvasWidth = 600;
		int ballWidth = 25;
		int ballHeight = 25;

		double ballX = 500;
		double ballY = 287.5;

		IPadCalculations rightPadCalculations = new RightPadCalculations(padWidth, padHeight, gap, 
				canvasWidth, canvasHeight, ballWidth, ballHeight,padSpeed);
		Pad pad = new Pad(rightPadCalculations);

		pad.setX(canvasWidth - 50 - padWidth);
		pad.setY(225);

		double expected = 180;
		double actual = pad.getAngleIfWasColisionWithBall(ballX, ballY);

		assertEquals(expected, actual, 0.1);
	}

	public void testRightPadColisionWithBallForBounceInBottomMidShouldReturn150() {
		int padWidth = 50;
		int padHeight = 150;
		int gap = 50;
		int padSpeed = 10;
		int canvasHeight = 400;
		int canvasWidth = 600;
		int ballWidth = 25;
		int ballHeight = 25;

		double ballX = 500;
		double ballY = 325;

		IPadCalculations rightPadCalculations = new RightPadCalculations(padWidth, padHeight, gap, 
				canvasWidth, canvasHeight, ballWidth, ballHeight,padSpeed);
		Pad pad = new Pad(rightPadCalculations);

		pad.setX(canvasWidth - 50 - padWidth);
		pad.setY(225);

		double expected = 150;
		double actual = pad.getAngleIfWasColisionWithBall(ballX, ballY);

		assertEquals(expected, actual, 0.1);
	}

	public void testRightPadColisionWithBallForBounceInUpEndShouldReturn240() {
		int padWidth = 50;
		int padHeight = 150;
		int gap = 50;
		int padSpeed = 10;
		int canvasHeight = 400;
		int canvasWidth = 600;
		int ballWidth = 25;
		int ballHeight = 25;

		double ballX = 500;
		double ballY = 200;

		IPadCalculations rightPadCalculations = new RightPadCalculations(padWidth, padHeight, gap, 
				canvasWidth, canvasHeight, ballWidth, ballHeight,padSpeed);
		Pad pad = new Pad(rightPadCalculations);

		pad.setX(200 - padWidth);
		pad.setY(225);

		double expected = 240;
		double actual = pad.getAngleIfWasColisionWithBall(ballX, ballY);

		assertEquals(expected, actual, 0.1);
	}

}
