package com.bobek.piotr.pong.model;

import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.user.client.ui.Image;

public class RightPadCalculations implements IPadCalculations {

	private final int WIDTH;
	private final int HEIGHT;
	private final int GAP;
	private final int CANVAS_WIDTH;
	private final int CANVAS_HEIGHT;
	private final int BALL_HEIGHT;
	private final int BALL_WIDTH;
	private final int MAX_BOUNCE_ANGLE = 60;
	private final int SPEED;

	private double y;
	private double x;
	private double yDest;
	private boolean inMove;	
	
	public RightPadCalculations(int width, int height, int gap, int canvasWidth,
			int canvasHeight, int ballWidth, int ballHeight, int speed) {
		this.WIDTH = width;
		this.HEIGHT = height;
		this.GAP = gap;
		this.CANVAS_WIDTH = canvasWidth;
		this.CANVAS_HEIGHT = canvasHeight;
		this.BALL_WIDTH = ballWidth;
		this.BALL_HEIGHT = ballHeight;
		this.SPEED = speed;		

		restart();
	}

	@Override
	public double getAngleIfWasColisionWithBall(double ballX, double ballY) {

		
		double halfOfPad = HEIGHT / 2; //polowa wysokosci paletki

		boolean yCollision = (ballY >= y || (ballY + BALL_HEIGHT) >= y);
		double halfOfBall = (double) BALL_HEIGHT / 2;
		boolean botCornerCollision = (y >= (halfOfBall + ballY) && (ballY < y));
		boolean upCornerCollision = ((y + HEIGHT) < (halfOfBall + ballY));

		double pointOfYCollision;

		if (botCornerCollision) {
			pointOfYCollision = ballY + BALL_HEIGHT;
		} else if (upCornerCollision) {
			pointOfYCollision = ballY - BALL_HEIGHT;
		} else {
			pointOfYCollision = ballY + halfOfBall;
		}

		if ((ballX + BALL_WIDTH) >= x && yCollision) {
			// dolna część pada
			if (pointOfYCollision > (halfOfPad + y)
					&& pointOfYCollision <= (HEIGHT + y)) {
				double pointOfColision = pointOfYCollision - halfOfPad - y;

				return 180 - (pointOfColision * MAX_BOUNCE_ANGLE) / halfOfPad;
			}
			// górna część pada
			else if (pointOfYCollision < (halfOfPad + y) && yCollision) {
				double pointOfColision = y + halfOfPad - pointOfYCollision;

				return 180 + ((pointOfColision * MAX_BOUNCE_ANGLE) / halfOfPad);
			} else if (pointOfYCollision == (y + halfOfPad)) {
				return 180;
			}
		}
		// brak kolizji
		else {
			return -1;
		}

		return -1;
	}

	@Override
	public void move() {
		
			if (yDest < y && y > 0) {
				y -= SPEED;
			} else if (yDest > y && y + HEIGHT < CANVAS_HEIGHT) {
				y += SPEED;
			}
		
	}

	@Override
	public void restart() {
		x = CANVAS_WIDTH - WIDTH - GAP;
		y = (CANVAS_HEIGHT - HEIGHT) / 2;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getyDest() {
		return yDest;
	}

	public void setyDest(double yDest) {
		this.yDest = yDest;
	}

	public boolean isInMove() {
		return inMove;
	}

	public void setInMove(boolean inMove) {
		this.inMove = inMove;
	}

	public int getWIDTH() {
		return WIDTH;
	}

	public int getHEIGHT() {
		return HEIGHT;
	}

	@Override
	public void setYDest(int cord) {
		this.yDest = cord;
	}
}
